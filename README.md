# git read-vim-undofile

Load the current history branch of a file edited by vim into a git repository.

# Usage

    usage: git vim-read-undofile [-h] -f FILE -u FILE [-n NAME]
                                 [--blobs | --trees] [-q]
    
    import a vim undofile in git
    
    optional arguments:
      -h, --help            show this help message and exit
      -f FILE, --file FILE  path to file
      -u FILE, --undo FILE  path to undofile
      -n NAME, --name NAME  filename in git
      --blobs               write only blobs
      --trees               write only blobs and trees
      -q, --quiet           show only last commmit hash

# Installation

    install git-vim-read-undofile /usr/bin
